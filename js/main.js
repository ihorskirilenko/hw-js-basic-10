'use strict'

/**
 * Реалізувати перемикання вкладок (таби) на чистому Javascript.
 *
 * Технічні вимоги:
 * У папці tabs лежить розмітка для вкладок. Потрібно, щоб після натискання на вкладку відображався конкретний текст для потрібної вкладки.
 * При цьому решта тексту повинна бути прихована. У коментарях зазначено, який текст має відображатися для якої вкладки.
 * Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.
 * Потрібно передбачити, що текст на вкладках може змінюватись, і що вкладки можуть додаватися та видалятися.
 * При цьому потрібно, щоб функція, написана в джаваскрипті, через такі правки не переставала працювати.
 * */

/**
 * 1st version, shit code but works :)
 * /

function tabSelection() {
    let navBar = document.querySelectorAll('.tabs-title');
    let tabs = document.querySelectorAll('.tabs-content > li');

    for (let i = 0; i < navBar.length; i++) {
        navBar[i].dataset.nmb = `${i}`;
        tabs[i].dataset.nmb = `${i}`;
        navBar[i].addEventListener('click', function () {
            for (let j = 0; j < navBar.length; j++) {
                navBar[j].classList.remove('active');
                tabs[j].classList.remove('active');
            }
            this.classList.add('active');
            document.querySelector(`.tabs-content li[data-nmb='${this.dataset.nmb}']`).classList.add('active');
        })
    }
}
*/

function tabSelection2() {
    let count = 0;
    let navBar = document.querySelectorAll('.tabs-title');
    let tabs = document.querySelectorAll('.tabs-content > li');
    let clRemove = function(el) {el.classList.remove('active')};

    tabs.forEach(it => {
        it.dataset.nmb = `${count}`;
        count++;
    })

    count = 0;
    navBar.forEach(el => {
        el.dataset.nmb = `${count}`;
        el.addEventListener('click', function() {
            navBar.forEach(el => clRemove(el));
            tabs.forEach(el => clRemove(el));
            this.classList.add('active');
            document.querySelector(`.tabs-content li[data-nmb='${this.dataset.nmb}']`).classList.add('active');
        })
        count++;
    })
}

/**
 * Event listener on 'load' gives possibility to set the 'active' class on first navigation item and
 * first paragraph automatically instead manually.
 * So, adding elements in HTML looks like a simple copy-paste.
 * */

window.addEventListener('load', () => {
    document.querySelector('.tabs-title').classList.add('active');
    document.querySelector('.tabs-content li').classList.add('active');
    tabSelection2();
});





